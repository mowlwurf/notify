<?php
/**
 * Created by JetBrains PhpStorm.
 * User: dnaumann
 * Date: 27.08.13
 * Time: 10:51
 * To change this template use File | Settings | File Templates.
 */

require_once 'vendor/autoload.php';

use NotifyLibraryBundle\Notify;

/**
 * modules
 * 1 syslog
 * 2 mail
 */

$chooseYourModule = 1;

switch($chooseYourModule){
    case 1:
    {
        $noteLog = new Notify('syslog');
        $noteLog->notify('testDummy');
        break;
    }
    case 2:
    {
        $mailSetup = array(
            'host'          => 'smtp.gmail.com',
            'port'          => 465,
            'encryption'    => 'ssl',
            'user'          => 'naumann.da@gmail.com',
            'userName'      => 'Daniel Naumann',
            'password'      => 'b4ck2th3r00tz'
        );

        $mailLog = new Notify('mail');
        $mailLog->setup($mailSetup);

        $mailOptions = array(
            'to'        => 'd.naumann@yatego.com',
            'subject'   => 'Test',
            'body'      => 'testDummy'
        );

        $mailLog->notify($mailOptions);
        break;
    }
}



