<?php
/**
 * Created by JetBrains PhpStorm.
 * User: dnaumann
 * Date: 28.08.13
 * Time: 00:10
 * To change this template use File | Settings | File Templates.
 */

namespace NotifyExtensionBundle;

class NotifyMailExtension {

    private $smtpHost = '';
    private $smtpPort = 25;
    private $smtpEncryption = '';

    private $smtpUser = '';
    private $smtpUserName = '';
    private $smtpPassword = '';

    private $transport = null;
    private $message = null;

    public function setup($parameters){
        $this->validateSetupParameters($parameters);
        $transport = new \Swift_SmtpTransport($this->smtpHost,$this->smtpPort,$this->smtpEncryption);
        $transport->setUsername($this->smtpUser);
        $transport->setPassword($this->smtpPassword);
        $this->setTransport($transport);
    }

    public function execute($mailParameters){
        $this->validateMessageParameters($mailParameters);
        $mail = new \Swift_Mailer($this->getTransport());
        if (!$mail->send($this->getMessage())) {
            throw new \ErrorException('Notifymail was not send','111');
        }
        return true;
    }

    private function validateMessageParameters($mail){
        $message = new \Swift_Message($mail['subject']);
        $message->setFrom(array('naumann.da@gmail.com' => 'Daniel'));
        $message->setTo(array($mail['to']));
        $message->setBody($mail['body']);
        $this->setMessage($message);
    }

    private function validateSetupParameters($parameters){
        var_dump($parameters);
        $this->setSmtpHost($parameters['host']);
        $this->setSmtpPort($parameters['port']);
        $this->setSmtpEncryption($parameters['encryption']);

        $this->setSmtpUser($parameters['user']);
        $this->setSmtpUserName($parameters['userName']);
        $this->setSmtpPassword($parameters['password']);
    }

    private function setSmtpEncryption($smtpEncryption)
    {
        $this->smtpEncryption = $smtpEncryption;
    }

    private function setSmtpHost($smtpHost)
    {
        $this->smtpHost = $smtpHost;
    }

    private function setSmtpPassword($smtpPassword)
    {
        $this->smtpPassword = $smtpPassword;
    }

    private function setSmtpPort($smtpPort)
    {
        $this->smtpPort = $smtpPort;
    }

    private function setSmtpUser($smtpUser)
    {
        $this->smtpUser = $smtpUser;
    }

    private function setTransport($transport)
    {
        $this->transport = $transport;
    }

    private function getTransport()
    {
        return $this->transport;
    }

    private function setMessage($message)
    {
        $this->message = $message;
    }

    private function getMessage()
    {
        return $this->message;
    }

    private function setSmtpUserName($smtpUserName)
    {
        $this->smtpUserName = $smtpUserName;
    }
}