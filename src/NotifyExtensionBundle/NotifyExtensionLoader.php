<?php
/**
 * Created by JetBrains PhpStorm.
 * User: dnaumann
 * Date: 27.08.13
 * Time: 02:26
 * To change this template use File | Settings | File Templates.
 */

namespace NotifyExtensionBundle;

/**
 * Class NotifyExtensionLoader
 * @package NotifyExtensionBundle
 */

class NotifyExtensionLoader {

    private static $bundleUsePath = '\NotifyExtensionBundle\\';

    private static $loadedExtensionModules = array();

    private static function getStableExtensionModules() {
        return array(
            'syslog' => 'NotifySyslogExtension',
            'mail'   => 'NotifyMailExtension',
            'db'     => 'NotifyDatabaseExtension'
        );
    }

    private static function getDevExtensionModules() {
        return array(
        );
    }

    public static function load($module){
        if (!in_array($module, array_keys(self::getdevExtensionModules())) && !in_array($module, array_keys(self::getStableExtensionModules()))) {
            throw new \ErrorException ('module isn\'t NotifyModule', '001');
        } elseif (in_array($module, self::getdevExtensionModules())) {
            return self::loaddev($module);
        }
        $moduleList = self::getStableExtensionModules();
        $module = self::$bundleUsePath.$moduleList[$module];
        if (in_array($module,array_keys(self::$loadedExtensionModules))) {
            return self::$loadedExtensionModules[$module];
        } else {
            self::$loadedExtensionModules[$module] = new $module;
            return self::$loadedExtensionModules[$module];
        }
    }

    private static function loaddev($module){
        $moduleList = self::getDevExtensionModules();
        $module = self::$bundleUsePath.$moduleList[$module];
        if (in_array($module,self::$loadedExtensionModules)) {
            return self::$loadedExtensionModules[$module];
        } else {
            self::$loadedExtensionModules[$module] = new $module;
            return self::$loadedExtensionModules[$module];
        }
    }

}