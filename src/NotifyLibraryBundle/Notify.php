<?php
/**
 * Created by JetBrains PhpStorm.
 * User: mowlwurf
 * Date: 27.08.13
 * Time: 01:02
 * To change this template use File | Settings | File Templates.
 */

namespace NotifyLibraryBundle;

use NotifyLibraryBundle\Authenticator;
use NotifyExtensionBundle\NotifyExtensionLoader;


class Notify {

    private $requestModule;

    private $requestToken;

    public function __construct($module){
        try {
            $auth = new Authenticator();
            $auth->authenticate($this->getRequestToken());
            $this->setRequestModule($module);


        } catch (\ErrorException $e) {
            print '['.$module.']['.$e->getCode().'] '.$e->getMessage();
        }
    }

    public function setup($parameters){
        $notifyExtension = $this->getRequestModule();
        $notifyExtension->setup($parameters);
    }

    public function notify($parameters){
        $notifyExtension = $this->getRequestModule();
        $notifyExtension->execute($parameters);
    }

    public function setRequestModule($requestModule)
    {
        $this->requestModule = $requestModule;
    }

    public function getRequestModule()
    {
        $module = NotifyExtensionLoader::load($this->requestModule);
        return $module;
    }

    public function getRequestToken()
    {
        return $this->requestToken;
    }


}